# Pleio leren
This repository contains the buildfiles for [leren.pleio.nl](https://leren.pleio.nl).

## Building
Before building the repository initialize the environment with:

    ./prepare-build.sh

This command downloads the latest version of Moodle. Then build a new Docker image with:

    docker build -t pleio/leren .

## Running
Run the container using

    docker run \
    -p 80:80 \
    -v ./config.example.php:/app/config.php \
    -v ./app-data:/moodledata \
    -e SMTP_HOST=172.17.0.1 \
    -e SMTP_HOSTHAME=pleio.nl \
    pleio/leren
