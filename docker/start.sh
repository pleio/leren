#!/bin/sh

echo "[i] Configuring according to environment variables..."
export SMTP_HOST=${SMTP_HOST:-172.17.0.1}
export SMTP_HOSTNAME=${SMTP_HOST:-pleio.nl}
envsubst < /etc/ssmtp/ssmtp.conf > /etc/ssmtp/ssmtp.conf

echo "[i] Starting daemon..."
httpd

# display logs
tail -F /var/log/apache2/*log
