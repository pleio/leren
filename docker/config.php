<?php  // Moodle configuration file

unset($CFG);
global $CFG;
$CFG = new stdClass();

$CFG->dbtype    = 'mariadb';
$CFG->dblibrary = 'native';
$CFG->dbhost    = (getenv('DB_HOST') ? getenv('DB_HOST') : '127.0.0.1');
$CFG->dbname    = (getenv('DB_NAME') ? getenv('DB_NAME') : 'moodle');
$CFG->dbuser    = (getenv('DB_USERNAME') ? getenv('DB_USERNAME') : 'moodle');
$CFG->dbpass    = (getenv('DB_PASSWORD') ? getenv('DB_PASSWORD') : 'changeme');
$CFG->prefix    = '';
$CFG->dboptions = array (
  'dbpersist' => 0,
  'dbport' => '',
  'dbsocket' => '',
  'dbcollation' => 'utf8mb4_general_ci',
);

$CFG->wwwroot   = (getenv('SITE_URL') ? getenv('SITE_URL') : 'http://www.example.com');
$CFG->dataroot  = '/app-data';
$CFG->admin     = 'admin';

$CFG->directorypermissions = 0777;

require_once(__DIR__ . '/lib/setup.php');

// There is no php closing tag in this file,
// it is intentional because it prevents trailing whitespace problems!
