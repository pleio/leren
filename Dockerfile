FROM php:7-alpine

RUN apk update \
    && apk add --no-cache \
    --allow-untrusted \
    openrc \
    ssmtp \
    gettext \
    php8 \
    php8-fileinfo \
    php8-apache2 \
    php8-iconv \
    php8-pecl-memcached \
    php8-mysqli \
    php8-session \
    php8-json \
    php8-xml \
    php8-curl \
    php8-zip \
    php8-zlib \
    php8-gd \
    php8-dom \
    php8-xmlreader \
    php8-mbstring \
    php8-openssl \
    php8-soap \
    php8-intl \
    php8-opcache \
    php8-tokenizer \
    php8-simplexml \
    #php8-phpunit \
    php8-ctype

RUN apk add php81-pecl-xmlrpc --repository http://dl-cdn.alpinelinux.org/alpine/edge/testing/ --no-cache --allow-untrusted

RUN apk upgrade

# gmp
RUN apk add --update --no-cache gmp gmp-dev \
    && docker-php-ext-install gmp

# Harmonise www-data uid with Ubuntu, as we mount storage from Ubuntu inside
RUN deluser xfs && deluser www-data \
    && adduser -D -u 33 -s /bin/false www-data

# Moodle
RUN wget https://download.moodle.org/download.php/direct/stable401/moodle-latest-401.zip -O temp.zip && \
    unzip temp.zip -d / && \
    mv /moodle /app && \
    rm temp.zip

RUN wget "https://moodle.org/plugins/download.php/28869/auth_saml2_moodle41_2022111701.zip" -O temp.zip && \
    unzip temp.zip -d / && \
    mv /saml2 /app/auth && \
    rm temp.zip

RUN wget "https://moodle.org/plugins/download.php/28375/theme_academi_moodle41_2023011900.zip" -O temp.zip && \
    unzip temp.zip -d / && \
    mv /academi /app/theme && \
    rm temp.zip

RUN mkdir -p /run/apache2 && \
    mkdir -p /app-data && \
    chown www-data:www-data /app-data && \
    chown -R www-data:www-data /app/enrol

COPY ./docker/apache2.conf /etc/apache2/conf.d/custom.conf
COPY ./docker/ssmtp.conf /etc/ssmtp/ssmtp.conf
COPY ./docker/php.ini /etc/php8/conf.d/100_custom.ini
COPY ./docker/config.php /app/config.php

COPY ./docker/start.sh /start.sh
RUN chmod +x /start.sh

WORKDIR /app

RUN chown -R 33:33 /app
EXPOSE 80
CMD ["/start.sh"]