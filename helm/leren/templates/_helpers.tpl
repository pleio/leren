{{- define "leren.name" -}}
{{- .Release.Name | trunc 63  -}}
{{- end }}

{{- define "leren.tlsSecretName" -}}
{{- printf "tls-%s" ((include "leren.name" .)) | trunc 63 | trimSuffix "-" }}
{{- end }}

{{- define "leren.secretsName" -}}
{{- printf "%s-secrets" ((include "leren.name" .)) | trunc 63 | trimSuffix "-" }}
{{- end }}

{{- define "leren.storageName" -}}
{{- if .Values.storage.existingDataStorage }}
{{- .Values.storage.existingDataStorage -}}
{{- else }}
{{- printf "%s-data" ((include "leren.name" .)) | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "leren.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "leren.labels" -}}
helm.sh/chart: {{ include "leren.chart" . }}
{{ include "leren.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "leren.selectorLabels" -}}
app.kubernetes.io/name: {{ include "leren.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

