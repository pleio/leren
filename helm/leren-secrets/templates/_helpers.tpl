{{- define "leren.name" -}}
{{- .Release.Name | trunc 63  -}}
{{- end -}}

{{- define "leren.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "leren.labels" -}}
helm.sh/chart: {{ include "leren.chart" . }}
{{ include "leren.selectorLabels" . }}
{{- end -}}

{{- define "leren.selectorLabels" -}}
app.kubernetes.io/name: {{ include "leren.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end -}}
